Mickey Petersen - Mastering Emacs Commands Spreadsheet
By: Alejandro Anzola (2017-08-06)

Chapter 3
   General:
      C-x C-f -> (find-file): Finds a file in the OS
      C-M-% -> (query-replace-regexp): ...
      C-x 8 P : Inserts the paragraph symbol (¶)

   Universal Arguments (page 47):
      C-u a : Prints 'aaaa' on the screen (4x)
      C-u C-u a : Prints 'aaaaaaaaaaaaaaaa' (16x)
      C-u C-u <... n-2 times> a : Prints 'a' 4^n = 2^(2n) times
      C-u 10 a : Prints 'aaaaaaaaaa' (10x)
      M-0 to M-9 : Digit argument 0 to 9
      C-0 to C-9 : Digit argument 0 to 9
      C-M-0 to C-M-9 : Digit argument 0 to 9
      C-- : Negative argument
      M-- : Negative argument
      C-M-- : Negative argument
      M-- M-d : kills the previous word before point
      C-- M-d : kills the previous word before point

   Discovering and Remembering Keys (page 50):
      <combination of keys> C-h : Displays help with a binding
      Ex:
	   C-x 8 C-h : Displays a computer-generated list of keys and their commands.

	   C-x 8 " : Prefix command
	   C-x 8 < : «
	   C-x 8 > : »
	   C-x 8 ? : ¿
	   C-x 8 C : ©
	   C-x 8 L : £
	   C-x 8 P : ¶
	   C-x 8 R : ®
	   C-x 8 S : §
	   C-x 8 Y : ¥

   The Customize Interface (page 54):
      M-x customize : Access the customize interface

   Evaluating Elips Code (page 59):
      M-x eval-buffer : will evaluate the entire buffer you're in.
      M-x eval-region : evaluates just the region that you have marked.
      
   Apropos (page 67):
      M-$ -> (ispell-word) : Check spelling of word under or before the cursor
      M-d -> (kill-word) : Kill characters forward until encountering the end of a word.
      C-<left> -> (left-word) : Move point N words to the left (right if negative)
      M-@ -> (mark-word) : Set mark ARG words away from point
      

Chapter 4
   General:
      C-x C-f : find (open) a file
      C-x C-s : Save the buffer
      C-x C-w : Saves buffer in different location
      C-x b : Switch buffer
      C-x k : Kill (close) a buffer
      C-x C-b : Display all open buffers
      C-x C-c : Exits Emacs
      ESC ESC ESC : Exits out of prompts, regions, prefix arguments and returns to just one window
      C-/ : Undo changes
      <F10> : Activates the menu bar.
   Window Management (page 90):
      C-x 0 : Deletes the active window
      C-x 1 : Deletes other windows
      C-x 2 : Split window below
      C-x 3 : Split window right
      C-x o : Switch active window

      Directional windows selection (page 92):
         required: having (windmove-default-keybinding) in init file
	 S-<left>, S-<right>, S-<up>, S-<down>
	 
      Working with other windows (page 92):
         C-x 4 C-f : Finds a file in the other window
	 C-x 4 d : Opens 'M-x dired' on the other windows
	 C-x C-o : Displays a buffer in the other window
	 C-x 4 b : Switches the buffer in the other window and makes it the active window
	 C-x 4 0 : Kills the buffer and window

      Frame Management (page 93):
         C-x 5 2 : Create a new frame
	 C-x 5 b : Switch buffer in other frame
	 C-x 5 0 : Delete active frame
	 C-x 5 1 : Delete other frames
	 C-x 5 C-f : Finds the file in the other window
	 C-x 5 C-o : Displays a buffer in the other window

      What if you don't want word wrapping? (page 101):
         M-x toggle-truncate-lines : Toggles word wrapping

      Sub- and Superword Movement(page 106):
         M-x subword-mode : Minor mode that treats 'CamelCase' as distinct words
	 M-x superword-mode : Minor mode that treats 'snake_case' as distinct words

      Moving by S-Expressions (page 108):
         C-M-f : Move forward by s-expression
	 C-M-b : Move backward by s-expression

      Moving by defun (page 116):
      	 C-M-a : Move to beginning of defun
	 C-M-e : Move to end of defun

      Scrolling (page 118):
         C-v : Scroll down one page
	 M-v : Scroll up one page
	 C-M-v : Scroll down the other window
	 C-M-S-v ¶ C-M-- C-M-v  : Scroll up the other window
	 C-x < : Scroll left
	 C-<next> : Scroll left
	 C-<prior> : Scroll right
	 C-x > : Scroll right
	 M-< : Move to the beginning of the buffer
	 M-> : Move to the end of the buffer

      Selections and Regions (page 124):
         C-<SPC> : Sets the mark, and toggles the region
	 C-u C-<SPC> : Jumps to the mark, and repeated calls go further back the mark ring
	 C-x C-x : Exchanges the point and mark, and reactivates your last region

      Searching and Indexing (page 133):
         C-s : Begins and incremental search
	 C-r : Begins a backward incremental search
	 C-M-s : Begins a regexp incremetal search
	 C-M-r : Begins a regexp backward incremetal search

	 <in search mode>:
	    M-n : Move to next item in search history
	    M-p : Move to previous item in search history

	    M-s c : Toggles case-sensitivity
	    M-s r : Toggles regular-expression mode
	    M-s w : Toggles word mode
	    M-s _ : Toggles symbol mode
	    M-s <SPC> : Toggles lax whitespace matching

	 C-s C-s : Begins Isearch against last search string
	 C-r C-r : Begins backward Isearch against last search string

Chapter 5
   General (page 168 - 169):
      C-d : Delete character
      <backspace> : Delete previous character
      M-d : Kill word
      C-k : Kill rest of line
      M-k : Kill sentence
      C-M-k : kill s-expression
      C-S-<backspace> : kill current line
      C-w : Kill active region (cut)
      M-w : Copy to kill ring (copy)
      C-M-w : Append kill
      C-y : Yank last kill (paste)
      M-y : Cycle through kill ring, replacing yanked text

   Transposing Text (page 174):
      C-t : Transpose characters
      M-t : Transpose words
      C-M-t : Transpose s-expressions
      C-x C-t : Transpose lines
      M-x transpose-paragraphs : Transpose paragraphs
      M-x transpose-sentences : Transpose sentences

   Filling and Commenting (page 181):
      M-q : Refills the paragraph point is in
      C-u M-q : Refills the paragraph point is in and Emacs will attempt to justify the text also
      C-x f : Sets the fill column width
      C-x . : Sets the fill prefix
      M-x auto-fill-mode : Toggles auto-filling

   Commenting (page 183):
      M-; : Comment or uncomment DWIM (Do What I Mean)
      C-x C-; : Comment or uncomment line
      M-x comment-box : Comments the region but as a box
      M-j ¶ C-M-j : Insert new line and continues with comment on a new line

   Search and Replace (page 185):
      C-M-% : Query regexp search and replace
      M-% : Query search and replace
      M-x replace-string : Search and replace
      M-x replace-regexp : Regexp search and replace

   Changing Case (page 194):
      C-x C-u : Uppercases the region
      C-x C-l : Lowercases the region
      M-x upcase-initials-region : Capitalizes the region
      M-c : Capitalizes the next word
      M-u : Uppercases the next word
      M-l : Lowercases the next word
      
   Counting things (page 197):
      M-x count-lines-region : Counts number of lines in the region
      M-x count-matches : Counts number of patterns that match in a region
      M-x count-words : Counts words, lines and chars in the buffer
      M-x count-words-region ¶ M-= : Counts words, lines and chars in the region

   Joining and Splitting Lines (page 200):
      C-o : Inserts and blank line after point
      C-x C-o : Deletes all blank lines after point
      C-M-o : Splits a line after point, keeping the indentation
      M-^ : Joins the line the point is on with the one above

   Sorting and Aligning (page 222):
      M-x sort-lines : Sorts alphabetically
      M-x sort-fields : Sorts fields lexicographically
      M-x sort-numeric-fields : Sorts fields numerically
      M-x sort-columns : Sorts columns alphabetically
      M-x sort-paragraphs : Sorts paragraphs alphabetically
      M-x sort-regexp-fields : Sorts by regexp-defined fields lexicographically

   Other Editing Commands:
      Zapping Characters (page 232):
         M-z : Kills up to (and including) the character that is typed

      Spell Checking (page 233):
         M-$ : Spell checks word at the point
	 M-x flyspell-mode : Minor mode that highlights spelling errors

Chapter 6
   General:
      M-x info ¶ C-h i : Open Emacs's Manual

   Working with Log files (page 244):
      C-x C-f : Finds a file
      C-x C-r : Finds a file in read only mode
      C-x C-q : Toggles read only mode
      M-x auto-revert-mode : Reverts buffer when file changes
      M-x auto-revert-tail-mode : Appends changes when file changes
      
